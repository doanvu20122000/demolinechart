package com.dd.company.testchart

import android.os.Bundle
import android.util.DisplayMetrics
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.dd.company.testchart.databinding.ActivityMainBinding
import com.github.mikephil.charting.animation.Easing
import com.github.mikephil.charting.components.AxisBase
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet


class MainActivity : AppCompatActivity() {
    companion object {
        const val YAXIS_VALUE_0 = 0f
        const val YAXIS_VALUE_5 = 5f
        const val YAXIS_VALUE_10 = 10f
        const val YAXIS_VALUE_15 = 15f
        const val YAXIS_VALUE_20 = 20f
        const val DAY_OF_MONTH = 31
    }

    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        binding.lineChart.apply {
            setOnClickListener(null)
        }
        setContentView(binding.root)
        setupChart()
    }

    private fun setMaxValueOfXAxis(maxValue: Int) {
        binding.lineChart.xAxis.labelCount = maxValue
    }

    private fun setupChart() {
        val lineChart = binding.lineChart
        with(lineChart) {
            animateX(800, Easing.EaseInSine)
            description.isEnabled = false
            setExtraOffsets(0f, 0f, 0f, 12f)
            xAxis.apply {
                position = XAxis.XAxisPosition.BOTTOM
                granularity = 1F
                setDrawGridLines(false)
                setMaxValueOfXAxis(DAY_OF_MONTH)
                textSize = 15f
//            valueFormatter = MyAxisFormatter()
            }
            axisRight.apply {
                isEnabled = false
            }
            axisLeft.apply {
//                setDrawGridLines(true)
                setDrawLabels(false)
                setDrawAxisLine(false)
                granularity = 5f
                mAxisMaximum = 25f
                mAxisMinimum = 0f
                enableGridDashedLine(20f, 10f, 0f)
            }
            legend.apply {
                isEnabled = false
//            orientation = Legend.LegendOrientation.VERTICAL
//            verticalAlignment = Legend.LegendVerticalAlignment.TOP
//            horizontalAlignment = Legend.LegendHorizontalAlignment.CENTER
//            textSize = 15F
//            form = Legend.LegendForm.LINE
            }
            setupHorizontalScroll()
        }
        setDataToLineChart()
    }

    private fun setupHorizontalScroll() {
        val displayMetrics = DisplayMetrics()
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.R) {
            @Suppress("DEPRECATION")
            display?.getRealMetrics(displayMetrics)
        } else {
            @Suppress("DEPRECATION")
            windowManager.defaultDisplay.getMetrics(displayMetrics)
        }
        val width = displayMetrics.widthPixels
        val defaultWidth = 20
        val defaultPie = 5
        if (width < defaultWidth * width / defaultPie) {
            binding.lineChart.layoutParams =
                LinearLayout.LayoutParams(defaultWidth * width / defaultPie, binding.lineChart.layoutParams.height)
        } else {
            binding.lineChart.layoutParams = LinearLayout.LayoutParams(width, binding.lineChart.layoutParams.height)
        }
    }

    private fun week1(): ArrayList<Entry> {
//        val drawableIcon = ContextCompat.getDrawable(this, R.drawable.phone_call_red)
        val sales = ArrayList<Entry>()
        sales.add(Entry(1f, YAXIS_VALUE_5))
        sales.add(Entry(2f, YAXIS_VALUE_10))
        sales.add(Entry(3f, YAXIS_VALUE_10))
        sales.add(Entry(4f, YAXIS_VALUE_5))
        sales.add(Entry(5f, YAXIS_VALUE_10))
        sales.add(Entry(6f, YAXIS_VALUE_15))
        sales.add(Entry(7f, YAXIS_VALUE_10))
        sales.add(Entry(8f, YAXIS_VALUE_20))
        sales.add(Entry(9f, YAXIS_VALUE_5))
        sales.add(Entry(10f, YAXIS_VALUE_0))
        sales.add(Entry(11f, YAXIS_VALUE_15))
        sales.add(Entry(12f, YAXIS_VALUE_10))
        sales.add(Entry(13f, YAXIS_VALUE_20))
        sales.add(Entry(14f, YAXIS_VALUE_5))
        sales.add(Entry(15f, YAXIS_VALUE_0))
        sales.add(Entry(16f, YAXIS_VALUE_15))
        sales.add(Entry(17f, YAXIS_VALUE_10))
        sales.add(Entry(18f, YAXIS_VALUE_20))
        sales.add(Entry(19f, YAXIS_VALUE_5))
        sales.add(Entry(20f, YAXIS_VALUE_0))
        sales.add(Entry(21f, YAXIS_VALUE_15))
        sales.add(Entry(22f, YAXIS_VALUE_10))
        sales.add(Entry(23f, YAXIS_VALUE_20))
        sales.add(Entry(24f, YAXIS_VALUE_5))
        sales.add(Entry(25f, YAXIS_VALUE_0))
        sales.add(Entry(26f, YAXIS_VALUE_15))
        sales.add(Entry(27f, YAXIS_VALUE_10))
        sales.add(Entry(28f, YAXIS_VALUE_20))
        sales.add(Entry(29f, YAXIS_VALUE_5))
        sales.add(Entry(30f, YAXIS_VALUE_0))
        sales.add(Entry(31f, YAXIS_VALUE_15))
        return sales
    }

    private fun setDataToLineChart() {

        val weekOneSales = LineDataSet(week1(), "Week 1")
        weekOneSales.apply {
            setDrawValues(false)
            setDrawCircles(true)
            setCircleColor(ContextCompat.getColor(this@MainActivity, R.color.teal_200))
            circleRadius = 3f
            setDrawCircleHole(false)
            lineWidth = 4f
            mode = LineDataSet.Mode.HORIZONTAL_BEZIER
            color = ContextCompat.getColor(this@MainActivity, R.color.aqua)
//            valueTextColor = ContextCompat.getColor(this, R.color.red)
        }
        val dataSet = ArrayList<ILineDataSet>()
        dataSet.add(weekOneSales)
        binding.lineChart.apply {
            data = LineData(dataSet)
            data.isHighlightEnabled = false
            invalidate()
        }
    }

    inner class MyAxisFormatter : IndexAxisValueFormatter() {

        private var items = (1..32).toList()
        override fun getAxisLabel(value: Float, axis: AxisBase?): String {
            val index = value.toInt()
            return if (index < items.size) {
                items[index].toString()
            } else "null"
        }
    }
}